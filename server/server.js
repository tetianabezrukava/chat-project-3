import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { PORT } from './config';
import routes from "./routes";

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes(app);

app.listen(PORT, () => console.log(`Listen port ${PORT}...`));