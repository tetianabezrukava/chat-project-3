import { Router } from "express";
import { USERS_FILES_PATH } from '../config';
import fs from 'fs';
import {uuid} from 'uuidv4';

const router = Router();

router
  .post("/register", (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data);
      const { email, login,  password } = req.body;
      const userByEmail = users.find(user => user.email === email);
      const userByLogin = users.find(user => user.login === login);
      if (userByEmail || userByLogin) {
        const error = userByEmail ? 'email' : 'login';
        res.status(400).json(
          {
            message: `There is user with such ${error}`
          }
        );
      } else {
        const userId = uuid();
        const user = login;
        const avatar = '';
        users.push({userId, user, avatar, email, login, password});
        fs.writeFile(USERS_FILES_PATH, JSON.stringify(users), () => {
          res.status(201).send(userId);
        });
      }
    })
  })
  .post("/login", (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data);
      const { login, password } = req.body;
      const userByLogin = users.find(user => user.login === login);
      if (userByLogin) {
        if (userByLogin.password === password) {
          const role = login === 'admin' ? 'admin' : 'user';
          delete userByLogin.login;
          delete userByLogin.password;
          
          res.status(200).json({ ...userByLogin, role })
        } else {
          res.status(400).json(
            {
              message: `Wrong password`
            }
          );
        }
      } else {
        res.status(404).json(
          {
            message: `There is no user with such login`
          }
        );
      }
    })
  })

export default router;
  