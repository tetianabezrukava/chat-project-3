import authRoutes from './authRoutes';
import usersRoutes from './usersRoutes';
import messagesRoutes from './messagesRoutes';

export default app => {
  app.use("/api/auth", authRoutes);
  app.use("/api/users", usersRoutes);
  app.use("/api/messages", messagesRoutes);
};