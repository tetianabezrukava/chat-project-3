import { Router } from "express";
import {MESSAGES_FILES_PATH, USERS_FILES_PATH} from '../config';
import fs from 'fs';
import {uuid} from 'uuidv4';

const router = Router();

router
  .get("/", (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data).map((user) => {
        delete user.login;
        delete user.password;
        return user
      });
      res.status(200).json(users)
    })
  })
  .get("/:id", (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data);
      const searchingUser = users.find(user => user.userId === req.params.id);
      if (searchingUser) {
        delete searchingUser.login;
        delete searchingUser.password;
        res.status(200).json(searchingUser);
      } else {
        res.sendStatus(404);
      }
    })
  })
  .patch('/:id', (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data);
      const searchingUser = users.find(user => user.userId === req.params.id);
      if (searchingUser) {
        const searchingUserIdx = users.findIndex(user => user === searchingUser);
        const newUser = [
          ...users.slice(0, searchingUserIdx),
          {...searchingUser, ...req.body},
          ...users.slice(searchingUserIdx + 1)
        ]
        fs.writeFile(USERS_FILES_PATH, JSON.stringify(newUser), () => {
          res.sendStatus(200);
        });
      } else {
        res.sendStatus(404);
      }
    })
  })
  .delete('/:id', (req, res) => {
    fs.readFile(USERS_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const users = JSON.parse(data);
      const searchingUser = users.find(user => user.userId === req.params.id);
      if (searchingUser) {
        const newUser = users.filter(user => user !== searchingUser)
        fs.writeFile(USERS_FILES_PATH, JSON.stringify(newUser), () => {
          res.sendStatus(200);
        });
      } else {
        res.sendStatus(404);
      }
    })
  })


export default router;