import { Router } from "express";
import { MESSAGES_FILES_PATH } from '../config';
import fs from 'fs';
import {uuid} from 'uuidv4';

const router = Router();

router
  .get("/", (req, res) => {
    res.status(200).sendFile(MESSAGES_FILES_PATH);
  })
  .get("/:id", (req, res) => {
    fs.readFile(MESSAGES_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const messages = JSON.parse(data);
      const searchingMessage = messages.find(message => message.id === req.params.id);
      if (searchingMessage) {
        res.status(200).json(searchingMessage);
      } else {
        res.sendStatus(404);
      }
    })
  })
  .post("/", (req, res) => {
    fs.readFile(MESSAGES_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const messages = JSON.parse(data);
      const id = uuid();
      messages.push({...req.body, id});
      fs.writeFile(MESSAGES_FILES_PATH, JSON.stringify(messages), () => {
        res.status(201)
      });
    })
  })
  .patch('/:id', (req, res) => {
    fs.readFile(MESSAGES_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const messages = JSON.parse(data);
      const searchingMessage = messages.find(messages => messages.id === req.params.id);
      if (searchingMessage) {
        const searchingMessageIdx = messages.findIndex(messages => messages === searchingMessage);
        const newItems = [
          ...messages.slice(0, searchingMessageIdx),
          {...searchingMessage, ...req.body},
          ...messages.slice(searchingMessageIdx + 1)
        ]
        fs.writeFile(MESSAGES_FILES_PATH, JSON.stringify(newItems), () => {
          res.sendStatus(200);
        });
      } else {
        res.sendStatus(404);
      }
    })
  })
  .delete('/:id', (req, res) => {
    fs.readFile(MESSAGES_FILES_PATH, 'utf8',(err, data) => {
      if (err) throw err;
      const messages = JSON.parse(data);
      const searchingMessage = messages.find(message => message.id === req.params.id);
      if (searchingMessage) {
        const newMessages = messages.filter(message => message !== searchingMessage)
        fs.writeFile(MESSAGES_FILES_PATH, JSON.stringify(newMessages), () => {
          res.sendStatus(200);
        });
      } else {
        res.sendStatus(404);
      }
    })
  })


export default router;