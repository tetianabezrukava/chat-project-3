import path from "path";

export const USERS_FILES_PATH = path.resolve(__dirname, 'api', 'users.json');
export const MESSAGES_FILES_PATH = path.resolve(__dirname, 'api', 'messages.json');

export const PORT = 3002;