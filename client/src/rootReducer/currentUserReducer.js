import {v4 as uuidv4} from 'uuid';

export default function currentUserReducer(state) {
  if (!state) {
    const id = uuidv4();
    return {
      userName: 'John',
      userId: id,
      userAvatar: 'https://padfieldstout.com/wp-content/uploads/2020/01/John_Easter_Padfield_Stout.jpg'
    }
  }
  
  return state
}