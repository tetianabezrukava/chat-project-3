import * as types from '../actionTypes';

export default function chatReducer(state, action) {
  if (!state) {
    return  {
      messages: [],
      likedMessages: [],
      editModal: false,
      editingMessage: {},
      preloader: true,
    }
  }
  
  const { type, payload } = action;
  const { messages, likedMessages } = state;
  
  let newMessages;
  
  switch (type) {
    case types.LOAD_MESSAGES:
      return {
        messages: [...payload],
        likedMessages: likedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
      
    case types.ADD_MESSAGE:
      return {
        messages: [...messages, payload],
        likedMessages: likedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
    
    case types.DELETE_MESSAGE:
      const deleteMessage = messages.find(item => item.id === payload);
      const deleteMessageIdx = messages.findIndex(item => item === deleteMessage);
      newMessages = [
        ...messages.slice(0, deleteMessageIdx),
        ...messages.slice(deleteMessageIdx + 1)
      ];
      return {
        messages: newMessages,
        likedMessages: likedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
      
    case types.OPEN_MODAL:
      return {
        messages: messages,
        likedMessages: likedMessages,
        editModal: true,
        editingMessage: messages.find(message => message.id === payload),
        preloader: false
      }
      
    case types.CLOSE_MODAL:
      return {
        messages: messages,
        likedMessages: likedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
      
    case types.EDIT_MESSAGE:
      const editMessageIdx = messages.findIndex(message => message.id === payload.id);
      newMessages = [
        ...messages.slice(0, editMessageIdx),
        payload,
        ...messages.slice(editMessageIdx + 1)
      ];
      return {
        messages: newMessages,
        likedMessages: likedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
      
    case types.LIKE_MESSAGE:
      let newLikedMessages
      if(likedMessages.includes(payload)) {
        const idx = likedMessages.indexOf(payload);
        newLikedMessages = [
          ...likedMessages.slice(0, idx),
          ...likedMessages.slice(idx + 1)
        ]
      } else {
        newLikedMessages = [...likedMessages, payload];
      }
      return {
        messages: messages,
        likedMessages: newLikedMessages,
        editModal: false,
        editingMessage: {},
        preloader: false
      }
      
    default:
      return state;
  }
}