import { combineReducers } from 'redux';

import chatReducer from './chatReducer';
import currentUserReducer from './currentUserReducer';

const rootReducer = combineReducers({
  chat: chatReducer,
  currentUser: currentUserReducer
});

export default rootReducer;