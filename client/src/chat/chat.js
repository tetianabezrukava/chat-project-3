import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';

import Services from '../services';
import { loadMessages, openModal  } from '../actions';

import Preloader from '../preloader';
import Header from '../header';
import MessageList from '../messageList';
import MessageInput from '../messageInput';
import EditMessageModal from '../editMessageModal/editMessageModal';

import './chatStyle.css';

function Chat({ url, data, loadMessages, openModal }) {
  const service = new Services();
  const { preloader, userId, messages, editModal } = data;
  const content = (
    <Fragment>
      <Header />
      <MessageList />
      <MessageInput />
      <EditMessageModal />
    </Fragment>
  )
  
  useEffect(() => {
    service.getData(url)
    .then((data) => {
      loadMessages(data);
    });
  }, [])
  
  useEffect(() => {
    window.addEventListener('keyup', (ev) => arrowUpHandler(ev));
    return () => window.removeEventListener('keyup', (ev) => arrowUpHandler(ev));
  });
  
  function arrowUpHandler(ev) {
    if (ev.key === 'ArrowUp' && !editModal) {
      const currentUserMessages = messages.filter(message => message.userId === userId);
      if (currentUserMessages.length) {
        const lastMessageId = currentUserMessages[currentUserMessages.length - 1].id;
        openModal(lastMessageId);
      }
    }
  }
  
  return(
    <div className='chat'>
      { preloader ? <Preloader /> : content }
    </div>
  )
}


const mapStateToProps = ({ chat : { preloader, messages, editModal }, currentUser : { userId } }) => {
  return ({
    data : {
      preloader,
      userId,
      messages,
      editModal
    }
  })
};

const mapDispatchToProps = (dispatch) => ({
  loadMessages: (payload) => dispatch(loadMessages(payload)),
  openModal: (payload) => dispatch(openModal(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);