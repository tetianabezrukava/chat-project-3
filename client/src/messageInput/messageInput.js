import React, { useState } from 'react';
import { connect } from 'react-redux'
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

import { addMessage } from '../actions';

import './messageInput.css';

function MessageInput({ currentUser, addMessage}) {
  const [ inputValue, setInputValue ] = useState('');
  const { userId, userAvatar, userName } = currentUser;
  const newMessage = {
      userId,
      id: uuidv4(),
      avatar: userAvatar,
      user: userName,
      text: inputValue,
      createdAt: moment().format(),
      editedAt: ''
  }
  
  const handleChange = (ev) => setInputValue(ev.target.value);

  const handleSubmit = (message) => {
    addMessage(message);
    setInputValue('');
  }
  
  return (
    <form
      className="message-input"
      onSubmit={(ev) => {
        ev.preventDefault();
        if (inputValue.trim()) {
          handleSubmit(newMessage)
        }
      }}
    >
      <input
        className="message-input-text"
        type="text"
        value={inputValue}
        onChange={handleChange}
      />
      <button
        className="message-input-button"
        type="submit"
      >
        Send
      </button>
    </form>
  )
}

const mapStateToProps = ({ currentUser }) => ({currentUser});

const mapDispatchToProps = (dispatch) => ({
  addMessage: (payload) => dispatch(addMessage(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageInput);
