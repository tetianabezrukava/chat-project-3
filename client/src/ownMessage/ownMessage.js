import React from 'react';
import {connect} from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

import './ownMessage.css'
import { deleteMessage, openModal } from '../actions';

function OwnMessage({ messageData, deleteMessage, openModal }) {
  const {id, text, time, isEdited} = messageData;
  const editClasses = isEdited ? 'edit-mark edited' : 'edit-mark';
  return (
    <div className="own-message">
      <div className="message-text">{text}</div>
      <div className="message-time">{time}</div>
      <div className={editClasses}>edited</div>
      <FontAwesomeIcon
        className="message-edit icon edit-icon"
        onClick={() => openModal(id)}
        icon={faPencilAlt}/>
      <FontAwesomeIcon
        className="message-delete icon delete-icon"
        onClick={() => deleteMessage(id)}
        icon={faTrash}/>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => ({
  deleteMessage: (payload) => dispatch(deleteMessage(payload)),
  openModal: (payload) => dispatch(openModal(payload))
});

export default connect(
  null,
  mapDispatchToProps
)(OwnMessage);
