import React from 'react';
import {connect} from 'react-redux';

import { getDataForHeader } from './headerHelpers';

import './header.css'

function Header({ messages }) {
  const { title, usersCount, messagesCount, lastMessageDate } = getDataForHeader(messages, 'Work');
  return (
    <div className='header'>
      <div className='header-title'>{title}</div>
      <div><span className='header-users-count'>{usersCount}</span> participants</div>
      <div><span className='header-messages-count'>{messagesCount}</span> messages</div>
      <div className="last-message">last message at <span className='header-last-message-date'>{lastMessageDate}</span></div>
    </div>
  )
}

const mapStateToProps = ({ chat: { messages } }) => ({messages});

export default connect(
  mapStateToProps
)(Header);