import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { closeModal, editMessage } from '../actions';

import './editMessageModal.css'

function EditMessageModal({ editModal, closeModal, editingMessage, editMessage }) {
  const [ inputValue, setInputValue ] = useState('');
  const classesList = editModal ? 'edit-message-modal modal-shown' : 'edit-message-modal';
  const handleChange = (ev) => setInputValue(ev.target.value);
  
  useEffect(() => {
    setInputValue(editingMessage.text)
  }, [editingMessage.text]);
  
  return (
    <div className={classesList}>
      <div className="modal-wrapper">
        <h3>Edit message</h3>
        <textarea
          className="edit-message-input"
          value={inputValue}
          onChange={handleChange}
        />
        <div className="buttons-group">
          <button
            className="edit-message-button"
            type="button"
            onClick={() => {
              if (editingMessage.text === inputValue) {
                closeModal();
                return;
              }
              editMessage({
                ...editingMessage,
                text: inputValue,
                editedAt: moment().format()
              })
            }}
          >Ok
          </button>
          <button
            className="edit-message-close"
            type="button"
            onClick={closeModal}
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = ({ chat: { editModal, editingMessage } }) => ({
  editModal,
  editingMessage
});

const mapDispatchToPros = (dispatch) => ({
  closeModal: () => dispatch(closeModal()),
  editMessage: (message) => dispatch(editMessage(message))
});

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(EditMessageModal);