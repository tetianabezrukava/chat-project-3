import  * as types from '../actionTypes';

const loadMessages = (messages) => ({
  type: types.LOAD_MESSAGES,
  payload: messages
});

const addMessage = (message) => ({
  type: types.ADD_MESSAGE,
  payload: message
});

const deleteMessage = (messageId) => ({
  type: types.DELETE_MESSAGE,
  payload: messageId
});

const openModal = (id) => ({
  type: types.OPEN_MODAL,
  payload: id
});

const closeModal = () => ({
  type: types.CLOSE_MODAL
});

const editMessage = (message) => ({
  type: types.EDIT_MESSAGE,
  payload: message
})

const likeMessage = (id) => ({
  type: types.LIKE_MESSAGE,
  payload: id
})

export {
  loadMessages,
  addMessage,
  deleteMessage,
  openModal,
  closeModal,
  editMessage,
  likeMessage
}