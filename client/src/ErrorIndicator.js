import React from 'react'

export default function ErrorIndicator() {
  return (
    <div className="error-message">
      <p>oops! something went wrong</p>
      <p>please, try again late</p>
    </div>
  )
}