import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons';

import { likeMessage } from '../actions';

import './message.css'

class Message extends Component {
  render() {
    const { id, userAvatar, userName, text, time, isLiked, isEdited } = this.props.messageData;
    const  likeClasses = isLiked ? 'heart-icon message-liked' : 'heart-icon message-like';
    const editClasses = isEdited ? 'edit-mark edited' : 'edit-mark';
    return (
      <div className="message">
        <img className="message-user-avatar" src={userAvatar} alt="avatar" />
        <div className="message-user-name">{userName}</div>
        <div className="message-text">{text}</div>
        <div className="message-time">{time}</div>
        <div className={editClasses}>edited</div>
        <FontAwesomeIcon
          onClick={() => this.props.likeMessage(id)}
          className={likeClasses}
          icon={faHeart} />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  likeMessage: (payload) => dispatch(likeMessage(payload))
});

export default connect(
  null,
  mapDispatchToProps
)(Message);