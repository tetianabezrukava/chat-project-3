import React, { useEffect, useRef } from 'react';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import { connect } from 'react-redux';

import Message from '../message';
import OwnMessage from '../ownMessage';

import './messageList.css';

function MessageList({ messages, likedMessages, currentUserId }) {
  const messagesEnd = useRef(null);
  
  useEffect(() => scrollToBottom(), [messages]);
  
  function scrollToBottom() {
    messagesEnd.current.scrollIntoView({ behavior: "smooth" });
  }
  
  const messageDates = messages.map((item) => moment(item.createdAt).format("dddd, DD MMMM YYYY"));
  const uniqDates = [...new Set(messageDates).values()] ;

  const content = uniqDates.map((data) => {
    let timeMark;
    switch (data) {
      case moment().format("dddd, DD MMMM YYYY"):
        timeMark = 'Today';
        break;
      case moment().subtract(1, 'days').format("dddd, DD MMMM YYYY"):
        timeMark = 'Yesterday';
        break;
      default:
        timeMark = data.slice(0, data.length - 5);
    }

    const content = (
      [...messages]
      .sort((itemA, itemB) => itemB - itemA)
      .filter(item => moment(item.createdAt).format("dddd, DD MMMM YYYY") === data)
      .map((item) => {
        const {id, avatar, user, userId, text, createdAt, editedAt} = item;
        const isCurrentUser = currentUserId === userId;
        let messageData;
        if (isCurrentUser) {
          messageData = {
            id,
            isEdited: editedAt,
            text: text,
            time: moment(createdAt).format('HH:mm')
          }
        } else {
          const isLiked = likedMessages.includes(id)
          messageData = {
            id,
            isLiked,
            isEdited: editedAt,
            userAvatar: avatar,
            userName: user,
            text: text,
            time: moment(createdAt).format('HH:mm')
          }
        }

        return (
          <li key={id}>
            {isCurrentUser ? <OwnMessage messageData={messageData} /> : <Message messageData={messageData} />}
          </li>
        )
      })
    )

    const key = uuidv4()

    return (
      <li key={key}>
        <span className="messages-divider">{timeMark}</span>
        <ul>
          {content}
        </ul>
      </li>
    )
  })

  return (
    <div className="message-list">
      <ul>
        {content}
      </ul>
      <div ref={messagesEnd} />
    </div>
  )
}

const mapStateToProps = ({ chat: { messages, likedMessages } , currentUser: { userId } }) => ({
  messages,
  likedMessages,
  currentUserId: userId
});

export default connect(
  mapStateToProps
)(MessageList);