import React from 'react';
import Chat from './chat';

export default function App() {
  return <Chat url="http://localhost:3002/api/messages" />
}